package com.ufro.regresion.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith({SpringExtension.class})
@SpringBootTest
class RegresionLinealTest {

    private static File DATA = Paths.get("src", "test", "resources", "data.json").toFile();
    List<Punto> puntos = new ArrayList<>();
    RegresionLineal regresion = new RegresionLineal();


    @BeforeEach
    void setUp() throws IOException {

        Punto[] puntosJson = new ObjectMapper().readValue(DATA, Punto[].class);
        puntos = Arrays.asList(puntosJson);
        regresion.setPuntos(puntos);

    }


    @Test
    void calcularPendiente() {
        double esperado = 0.019;
        Assertions.assertEquals(esperado,regresion.calcularPendiente(),0.1);
    }

    @Test
    void calcularInterseccion(){
        regresion.calcularPendiente();
        double esperado = 0.334;
        Assertions.assertEquals(esperado,regresion.calcularInterseccion(),0.1);
    }


    @Test
    void obtenerEcuacion(){

        regresion.calcularPendiente();
        regresion.calcularInterseccion();
        regresion.formatearEcuacion();

        String ecuacionEsperada = "y = 0.334 + 0.019 * x";
        Assertions.assertEquals(ecuacionEsperada, regresion.getRegresion());
    }

}