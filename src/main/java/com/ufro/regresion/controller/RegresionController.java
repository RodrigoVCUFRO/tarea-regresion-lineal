package com.ufro.regresion.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ufro.regresion.model.Punto;
import com.ufro.regresion.model.RegresionLineal;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class RegresionController {
    @PostMapping(value = "/obtener_rl", headers = "Accept=application/json",
            produces = "application/json", consumes = "application/json")
    public ResponseEntity<?> obtenerRegresionLineal(@RequestBody String puntosJson) throws Exception {

        Punto[] puntosTmp = new ObjectMapper().readValue(puntosJson, Punto[].class);
        List<Punto> puntos = Arrays.asList(puntosTmp);
        RegresionLineal regresion = new RegresionLineal();
        regresion.setPuntos(puntos);
        regresion.calcularPendiente();
        regresion.calcularInterseccion();
        regresion.formatearEcuacion();

        return ResponseEntity.status(HttpStatus.OK).body(regresion.getRegresion());

    }
}
