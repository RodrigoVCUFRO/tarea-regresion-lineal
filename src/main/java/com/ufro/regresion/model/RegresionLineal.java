package com.ufro.regresion.model;

import lombok.Data;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Data
public class RegresionLineal {
    /**
     * puntos de coordenadas
     */
    private List<Punto> puntos = new ArrayList<>();

    /**
     * formula resultante
     */
    private String regresion;
    /**
     * Interseccion con el eje X
     */
    private double interseccionX;
    /**
     * pendiente de la formula
     */
    private double pendiente;

    /**
     * calcular pendiente.
     */
    public double calcularPendiente(){
        double sumX = 0.0;
        double sumY = 0.0;
        double sumXY = 0.0;
        double sumXX = 0.0;
        for (Punto punto : this.puntos){
            sumX += punto.getCoordenadaX();
            sumY += punto.getCoordenadaY();
            sumXY += punto.getCoordenadaX() * punto.getCoordenadaY();
            sumXX += Math.pow(punto.getCoordenadaX(),2);
        }
        this.setPendiente(((this.puntos.size() * sumXY)-(sumX * sumY))/((this.puntos.size() * sumXX) - Math.pow(sumX,2)));
        return this.pendiente;

    }
    /**
     * calcular la interseccion con el eje X
     * como y = a + px entonces -> a = y - px, donde X e Y son el promedio de sus valores.
     */
    public double calcularInterseccion() {
        double promedioX = puntos.stream().mapToDouble(Punto::getCoordenadaX).average().getAsDouble();
        double promedioY = puntos.stream().mapToDouble(Punto::getCoordenadaY).average().getAsDouble();
        this.interseccionX = promedioY - (this.pendiente * promedioX);
        return this.interseccionX;
    }
    /**
     * formato de la formula
     */
    public void formatearEcuacion() {
        NumberFormat nf = NumberFormat.getInstance(new Locale("en", "US"));
        this.regresion = "y = " + nf.format(this.getInterseccionX()) + " + " + nf.format(this.getPendiente()) + " * x";

    }
}
