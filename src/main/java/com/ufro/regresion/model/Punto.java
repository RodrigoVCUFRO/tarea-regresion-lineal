package com.ufro.regresion.model;

import lombok.Data;

@Data
public class Punto {
    /**
     * Coordenada X
     */
    private double coordenadaX;

    /**
     * Coordenada Y
     */
    private double coordenadaY;
}
