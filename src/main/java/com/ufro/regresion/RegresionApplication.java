package com.ufro.regresion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegresionApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegresionApplication.class, args);
	}

}
